package com.example.demo.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.IUserService;

@Service
public class UserServiceImpl implements IUserService{
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User createUser(User newUser) {
		try {
			System.out.println(" User in service :"+ newUser);
			userRepository.save(newUser);
			return userRepository.save(newUser);
		} catch (Exception e) {
			System.out.println("Error Occured in User Service "+e);
		}
		return null;
	}

	@Override
	public User getUserById(Long id) {
		return userRepository.findById(id).get();
	}

	@Override
	public User updateUser(User newUser, Long id) {
		User userDb = userRepository.findById(id).get();
		userDb.setName(newUser.getName());
		return userRepository.save(userDb);
	}

	@Override
	public String deleteUser(Long id) {
		try {
			userRepository.deleteById(id);
			boolean isFound = userRepository.existsById(id);
			if(!isFound) {
				return "Deleted Successfully...";
			}	
		} catch (Exception e) {
			return "Deletion Failed !!! " + e.toString() ;
		}
		return "Record not Deleted !!";
	}

	@Override
	public String delete() {
		userRepository.deleteAll();
		if(userRepository.count() == 0) {
			return "All Records Deleted Successfully..";
		}
		return "All Records Deletion Failed !!!";
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public Long getAllUsersCount() {
		return userRepository.count();
	}
}
