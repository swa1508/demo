package com.example.demo.rest;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.model.User;

public interface IUserRest {
	
	/**1. Hello Call **/
	@GetMapping("/hello")
	public String Hello();
	
	/**2. user create call**/
	@PostMapping("/createUser")
	public User createUser (@RequestBody User newUser);
	
	/**3. find user by id  call**/
	@GetMapping("/getUserById/{id}")
	public User getUserById(@PathVariable ("id") Long id);
	
	/**4. update User call**/
	@PostMapping("/updateUser/{id}")
	public User updateUser (@RequestBody User newUser,@PathVariable("id") Long id);
	
	/**5. Delete User by id call**/
	@DeleteMapping("/deleteUser/{id}")
	public String deleteUser(@PathVariable ("id") Long id);
	
	/**6. Delete All User call**/
	@DeleteMapping("/deleteAllUsers")
	public String deleteUser();
	
	/**7. Show All User call**/
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers();
	
	/**8. Show All User call**/
	@GetMapping("/getAllUsersCount")
	public Long getAllUsersCount();
	
}
