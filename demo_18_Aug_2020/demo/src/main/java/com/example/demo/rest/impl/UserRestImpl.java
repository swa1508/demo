package com.example.demo.rest.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.rest.IUserRest;
import com.example.demo.service.IUserService;

@RequestMapping("/user")
@RestController
public class UserRestImpl implements IUserRest{
	
	private static final Logger logger = LoggerFactory.getLogger(UserRestImpl.class);
	
	@Autowired
	private IUserService userService; 
	
	/** 1. Sample Hello rest call**/
	@Override
	public String Hello() {
		return "Hello swatantra";
	}
	
	/** 2. User Create call**/
	@Override
	public User createUser (User newUser) {
		User userDb = null;
		System.out.println(" User in rest :"+ newUser);
		try {
			userDb = userService.createUser(newUser);
		} catch (Exception e) {
			logger.error("Error is : {}",e);
			System.out.println("Error occured in UserRestImpl : "+e);
		}
		return userDb;
	}

	/**3. find user by id  call**/
	@Override
	public User getUserById(Long id) {
		return userService.getUserById(id);
	}

	/**4. update User call**/
	@Override
	public User updateUser(User newUser, Long id) {	
		return userService.updateUser(newUser,id);
	}

	/**5. Delete User by id call**/
	@Override
	public String deleteUser(Long id) {
		return userService.deleteUser(id);
	}

	/**6. Delete All User call**/
	@Override
	public String deleteUser() {
		return userService.delete();
	}

	/**7. Show All User call**/
	@Override
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@Override
	public Long getAllUsersCount() {
		return userService.getAllUsersCount();
	}	

}
