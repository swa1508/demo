package com.example.demo.service;

import java.util.List;

import com.example.demo.model.User;

public interface IUserService {

	User createUser(User newUser);

	User getUserById(Long id);

	User updateUser(User newUser, Long id);

	String deleteUser(Long id);

	String delete();

	List<User> getAllUsers();

	Long getAllUsersCount();

}
